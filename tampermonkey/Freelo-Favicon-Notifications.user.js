// ==UserScript==
// @name         Freelo Favicon Notifications
// @namespace    none
// @version      0.1
// @description  Freelo favicon notifications
// @author       You
// @match        https://app.freelo.cz/*
// @grant        none
// @require      http://lab.ejci.net/favico.js/favico.min.js
// @require      http://code.jquery.com/jquery-latest.js
// ==/UserScript==

(function() {
    'use strict';

    function mylog(msg) {
        if (window.console && window.console.log) {
            console.log("FFN: " + msg);
        }
    }

    mylog("Script started...")

    var favicon = new Favico({
        position  :'up',
        animation :'none',
        bgColor   :'#dd2c00',
        textColor :'#fff0e2',
        element   :$("link[href$='/front/images/icons/favicon-32x32.png']")[0]
    });

    function getNotificationsNum() {
        var el = $("#js-notifications-count")
        return el ? el.text() : 0;
    }

    function showFaviconBadge() {
        var num = getNotificationsNum();
        mylog("Notification count found " + num)
        if (num == 0) {
            favicon.reset();
        }
        else {
            favicon.badge(num);
        }
    }

    function newUpdate() {
        mylog("Timer started...")
        setInterval(showFaviconBadge, 2000);
    }

    newUpdate();
})();