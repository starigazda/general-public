// ==UserScript==
// @name Trello utils
// @namespace https://trello.com/
// @version 0.0
// @description Trello utils
// @match https://trello.com/b/*
// @require http://code.jquery.com/jquery-latest.js
// @author Msta
// ==/UserScript==

$(document).ready(function() {
    'use strict';

    // case insensitive contains
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function( elem ) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    $("div.board-menu").addClass("hide");
    $("div.board-wrapper").removeClass("is-show-menu");

    var setListBackground = function() {
        console.log("Background...");

        var red = '#fccbc2',
            blue = '#9ec4ff',
            green = '#dbffd1',
            grey = '#c4c4c4',
            black = '#898989',
            white = '#fff',
            yellow = '#ffd800',
            darkred = '#ff6363',
            lightblue = '#dbf3ff',
            lightorange = '#ffd396',
            darkdarkred = '#c1150f',
            pink = '#fabaff',
            orange = '#ff8300';

        $('body').hover(function() {
            $("h2:contains('IN PROGRESS')").css('color', black).parents('.list').css('background', lightorange);
            $("h2:contains('BLOCKED')").css('color', white).parents('.list').css('background', red);
            $("h2:contains('DONE')").css('color', white).parents('.list').css('background', green);
        });
    };

    var trelloCToolsChecker = function() {
        console.log("Chaos tools...");

        // Show card IDs.
        $('.card-short-id.hiden')
            .removeClass('hidden');

        // Show card counts.
        $('.list-header-num-cards.hide')
            .removeClass('hide');

        // Add card ID on card window.
        var $cardDetailIdEl = $('.card-detail-window .window-title .card-detail-card-id');
        if ($cardDetailIdEl.length < 1) {
            $cardDetailIdEl = $('<span />').addClass('card-detail-card-id').prependTo('.card-detail-window .window-title');
        }
        var cardId = '#' + window.location.href.split('/').pop().split('-').shift();
        $cardDetailIdEl.text(cardId);
        if ($cardDetailIdEl.length > 0 && !window.document.title.match(/^#(\d+)/)) {
            window.document.title = cardId + ' ' + window.document.title;
        }

    };

    var workloadCpllector = function() {
        console.log("Collecting workload...");
        $('div.list-wrapper')
            .each(function(index) {
            var listname = $(this).find('textarea.list-header-name').text();
            let workload = 0;
            $(this).find(".list-card-details").each(function(index) {

                var relevant = $(this).find("img.member-avatar[title*='starigazda']");
                if (relevant.length == 0) return;

                $(this).find("span.badge-text:contains('workload')").each(function(index) {
                    var badgeText = $(this).text();
                    workload += parseFloat(badgeText.split(':').pop());
                });

            });

            if (workload == 0) return;
            var tmpEl = $(this).find("p.list-header-num-cards");
            var tmpText = tmpEl.text().split(",");
            var newText = tmpText[0] + ", " + workload + " workload";
            console.log(newText);
            tmpEl.html(newText);
        });
    };

    var css = '';
    css += '.card-short-id, .card-detail-card-id { display: inline-block; padding-right: .6em; font-weight: bold; color: #888; }';
    $('body').append('<style>' + css + '</style>');

    var periodicFunction =  function() {
        workloadCpllector();
        trelloCToolsChecker();
    }

    setInterval(periodicFunction, 2000);
    setListBackground();

});